package id.co.firzil.protelindopmmobile.entity;

import android.database.Cursor;

import com.gz.databaselibrary.GzEntity;
import com.gz.databaselibrary.Utils;
import com.gz.databaselibrary.annotation.Column;
import com.gz.databaselibrary.annotation.Id;
import com.gz.databaselibrary.annotation.Table;

import java.util.ArrayList;
import java.util.List;

import static com.gz.databaselibrary.GzApplication.getInstance;

/**
 * Created by Rio Rizky Rainey on 03/02/2015.
 */
@Table(name = "task_list_overview")
public class TaskListOverview extends GzEntity{

    public final static int PERIODE_SEKARANG = 0;
    public final static int PERIODE_KEMARIN = 1;

    @Id
    @Column(name="pmp_status")
    private String pmpStatus;

    @Column(name="count")
    private Integer count;

    @Column(name = "periode")
    private Integer periode;

    public TaskListOverview() {
    }

    public TaskListOverview(String pmpStatus, Integer count) {
        this.pmpStatus = pmpStatus;
        this.count = count;
    }

    /**
     * @param periodeKemarin 1 jika periode kemarin. 0 jika periode sekarang
     * @return list tasklistoverview
     */

    public static void kosongkanByPeriode(int periodeKemarin) {
        String tableName = TaskListOverview.class.getAnnotation(Table.class).name();
        getInstance().getOpenHelper().getDatabase().delete(tableName, "periode=?", new String[]{periodeKemarin + ""});
    }

    public static List<TaskListOverview> getListByPeriode(int periodeKemarin) {
        String tableName = TaskListOverview.class.getAnnotation(Table.class).name();
        String sql = "SELECT * FROM " + tableName + " WHERE periode=?";
        Cursor cursor = getInstance().getOpenHelper().getDatabase().rawQuery(sql, new String[]{periodeKemarin + ""});
        List<TaskListOverview> list = new ArrayList<>();
        try {
            while (cursor.moveToNext()) {
                @SuppressWarnings("unchecked")
                TaskListOverview entity = Utils.setEntityFromCursor(TaskListOverview.class, cursor);
                list.add(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return list;
    }

    public String getPmpStatus() {
        return pmpStatus;
    }

    public void setPmpStatus(String pmpStatus) {
        this.pmpStatus = pmpStatus;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getPeriode() {
        return periode;
    }

    public void setPeriode(Integer periode) {
        this.periode = periode;
    }

    @Override
    public void saveMerge() {
        TaskListOverview taskList = findById(getClass(), getId());
        if (taskList != null && taskList.getPeriode() != 0)
            periode = taskList.getPeriode();
        if (periode == null) {
            periode = 0;
        }
        super.saveMerge();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskListOverview that = (TaskListOverview) o;

        if (pmpStatus != null ? !pmpStatus.equals(that.pmpStatus) : that.pmpStatus != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return pmpStatus != null ? pmpStatus.hashCode() : 0;
    }
}
