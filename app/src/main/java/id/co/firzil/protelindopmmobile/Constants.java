package id.co.firzil.protelindopmmobile;

import android.os.Environment;

/**
 * Created by Rio Rizky Rainey on 21/02/2015.
 * rizkyrainey@gmail.com
 */
public class Constants {

    public final static String FILE_IMAGE = Environment.getExternalStorageDirectory() + "/.ProtelindoPM/"; //Do not edit unless you know what you are doing
    public final static String DATE_FORMAT = "MM/dd/yyyy-kk:mm:ss"; //Do not edit unless you know what you are doing
    public final static String SENDER_ID = "protelindo-pm-1"; //Do not edit unless you know what you are doing

    public final static double DISTANCE = 200; //Do not edit unless you know what you are d8oing
}
