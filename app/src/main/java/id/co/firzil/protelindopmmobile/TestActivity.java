package id.co.firzil.protelindopmmobile;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.gz.databaselibrary.ConnectionListener;
import com.gz.databaselibrary.ServiceHandler;

/**
 * Created by Rio Rizky Rainey on 02/03/2015.
 * rizkyrainey@gmail.com
 */
public class TestActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ServiceHandler serviceHandler = new ServiceHandler(this);
        serviceHandler.addParam("email", "hai@hai.com");
        serviceHandler.addParam("gcm_id", "jsadjksnadolsjakdjnsaodjasidjasdsa");
        serviceHandler.setConnectionListener(new ConnectionListener() {
            @Override
            public void onBeforeRequest() {
            }

            @Override
            public void onAfterRequested(String response) {
                Log.d("", "" + response);
            }
        });
        serviceHandler.makeServiceCallAsyncTask("http://103.28.22.45/boral/public_html/api/authenticate", ServiceHandler.POST);
    }
}
