package id.co.firzil.protelindopmmobile.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.gz.databaselibrary.ConnectionListener;
import com.gz.databaselibrary.ServiceHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import id.co.firzil.protelindopmmobile.BaseAdapter;
import id.co.firzil.protelindopmmobile.DB;
import id.co.firzil.protelindopmmobile.R;
import id.co.firzil.protelindopmmobile.Tag;
import id.co.firzil.protelindopmmobile.URL;
import id.co.firzil.protelindopmmobile.Utils;
import id.co.firzil.protelindopmmobile.entity.StatusHistory;
import id.co.firzil.protelindopmmobile.entity.TaskList;

/**
 * Created by Rio Rizky Rainey on 16/03/2015.
 * rizkyrainey@gmail.com
 */
public class StatusHistoryFragment extends BaseActivity {

    public static final String TAG = "PM Status History";
    private TaskList taskList;

    private ListView historyListView;

    private StatusHistoryAdapter historyAdapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        taskList = (TaskList) getIntent().getSerializableExtra("taskList");

        setContentView(R.layout.fragment_status_history);
        setNavbar();
        setTitle(TAG);
        historyListView = (ListView) findViewById(R.id.listView_statusHistory);
        loadHistory();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.main:
                loadHistory();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadHistory() {
        historyAdapter = new StatusHistoryAdapter(c);
        historyAdapter.add(StatusHistory.findByIdPm("" + taskList.getIdPm()));
        historyListView.setAdapter(historyAdapter);
        if (Utils.isOnline(StatusHistoryFragment.this)) {
            ServiceHandler serviceHandler;
            if (StatusHistory.isHasData("" + taskList.getIdPm())) {
                serviceHandler = new ServiceHandler(StatusHistoryFragment.this, false);
            } else {
                serviceHandler = new ServiceHandler(StatusHistoryFragment.this, "Download history from server");
            }
            serviceHandler.addParam(Tag.IDTASK, "" + taskList.getIdPm());
            serviceHandler.setConnectionListener(new ConnectionListener() {
                @Override
                public void onBeforeRequest() {
                }

                @Override
                public void onAfterRequested(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.optInt(Tag.FLAG) == 1) {
                            JSONArray jsonArray = jsonObject.getJSONArray(Tag.RESULT);
                            List<StatusHistory> historyList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                StatusHistory sh = new StatusHistory();

                                int id_history = object.getInt("idpm_history");

                                //statusHistory.initFromJSONObject(object);
                                sh.setStatus(object.getString("pmh_status"));
                                sh.setDate(object.getString("pmh_date"));
                                sh.setIdStatusHistory(id_history);
                                sh.setIdPm(taskList.getIdPm());
                                sh.setAssign(object.getJSONObject("action_by_data").getString("level"));
                                sh.setDescription(object.optString("pmh_remark"));

                                DB.get(c).saveAndMergeStatusHistory(sh.getIdStatusHistory()+"", sh.getIdPm()+"", sh.getDate(),
                                        sh.getStatus(), sh.getDescription(), sh.getAssign());

                                historyList.add(sh);
                            }
                            historyAdapter = new StatusHistoryAdapter(c);
                            historyAdapter.add(historyList);
                            historyListView.setAdapter(historyAdapter);
                        }
                    } catch (Exception e) {
                        Utils.makeToast(StatusHistoryFragment.this, "Koneksi bermasalah saat mengambil data ke server");
                        e.printStackTrace();
                    }
                }
            });
            addServiceHandler(serviceHandler);
            serviceHandler.makeServiceCallAsyncTask(URL.TASK_HISTORY, ServiceHandler.GET);
        } else
            Utils.makeToast(StatusHistoryFragment.this, "Koneksi bermasalah saat mengambil data ke server");
    }

//    @Override
//    public String getTagFragment() {
//        return TAG;
//    }

    public void setTaskList(TaskList taskList) {
        this.taskList = taskList;
    }

    private class StatusHistoryAdapter extends BaseAdapter<StatusHistory> {

        private boolean addList;

        public StatusHistoryAdapter(Context context) {
            super(context);
        }

        @Override
        public void add(StatusHistory entity) {
            int index = indexOf(entity);
            if (index != -1) {
                getList().add(index + 1, entity);
                getList().remove(index + 1);
            } else {
                getList().add(entity);
            }
            if (!addList) {
                sortByDate();
                notifyDataSetChanged();
            }
        }

        @Override
        public void add(List<? extends StatusHistory> listAdd) {
            addList = true;
            for (int i = 0; i < listAdd.size(); i++) {
                add(listAdd.get(i));
            }
            notifyDataSetChanged();
            addList = false;
            sortByDate();
        }

        public void sortByDate() {
            Collections.sort(getList(), new Comparator<StatusHistory>() {
                public int compare(StatusHistory o1, StatusHistory o2) {
                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss", Locale.ENGLISH);
                    try {
                        Date dateO1 = format.parse(o1.getDate());
                        Date dateO2 = format.parse(o2.getDate());
                        return dateO2.compareTo(dateO1);
                    } catch (ParseException e) {
                        e.printStackTrace();
                        return 0;
                    }
                }
            });
        }

        @Override
        public View getView(int index, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = getInflater().inflate(R.layout.sample_card_list_status_history, parent, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            StatusHistory s = getItem(index);
            holder.dateTextView.setText(s.getDate());
            holder.assignTextView.setText(s.getAssign());
            holder.statusTextView.setText(s.getStatus());
            holder.descTextView.setText(TextUtils.isEmpty(s.getDescription()) || s.getDescription().equalsIgnoreCase("null")
                    ? "" : s.getDescription());
            return convertView;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        }

        public class ViewHolder {

            TextView dateTextView;
            TextView assignTextView;
            TextView statusTextView;
            TextView descTextView;

            public ViewHolder(View itemView) {
                dateTextView = (TextView) itemView.findViewById(R.id.textView_date);
                assignTextView = (TextView) itemView.findViewById(R.id.textView_assign);
                statusTextView = (TextView) itemView.findViewById(R.id.textView_status);
                descTextView = (TextView) itemView.findViewById(R.id.textView_desc);
            }
        }
    }
}
