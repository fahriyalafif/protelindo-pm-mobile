package id.co.firzil.protelindopmmobile;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.gz.databaselibrary.ConnectionListener;
import com.gz.databaselibrary.ServiceHandler;

import org.json.JSONObject;

public class ForgotPasswordActivity extends AppCompatActivity {

    private EditText emailEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_forgot_password);

        emailEditText = (EditText) findViewById(R.id.editText_email);
        findViewById(R.id.button_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickForgotPassword(view);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_forgot_password, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onClickForgotPassword(View view) {
        ServiceHandler serviceHandler = new ServiceHandler(this, getResources().getString(R.string.tunggu));
        serviceHandler.addParam(Tag.EMAIL, emailEditText.getText().toString());
        serviceHandler.setConnectionListener(new ConnectionListener() {
            @Override
            public void onBeforeRequest() {

            }

            @Override
            public void onAfterRequested(String response) {
//                throw new Error("not yet implemented/nunggu api");
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Utils.makeToast(ForgotPasswordActivity.this, jsonObject.optString(Tag.MSG, "Terjadi kesalahan dalam menghubungi server..."));
                } catch (Exception e) {
                    e.printStackTrace();
                    Utils.makeToast(ForgotPasswordActivity.this, "Maaf response server bukan json, silakan coba lagi lain kali.");
                }
            }
        });
        serviceHandler.makeServiceCallAsyncTask(URL.FORGET_PASSWORD, ServiceHandler.POST);
    }
}
