package id.co.firzil.protelindopmmobile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rio Rizky Rainey on 28/02/2015.
 * rizkyrainey@gmail.com
 */
public abstract class BaseAdapter<T> extends android.widget.BaseAdapter implements AdapterView.OnItemClickListener {

    protected List<T> list;
    private Context context;
    private LayoutInflater inflater;
    private boolean addList;

    public BaseAdapter(Context context) {
        list = new ArrayList<>();
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    /**
     * get activity
     */
    public Context getContext() {
        return context;
    }

    /**
     * @return inflater
     */
    public LayoutInflater getInflater() {
        return inflater;
    }

    /**
     * tambah ke array list parent. sebelumnya Object T harus override function
     * equals() yang sudah disediakan oleh java. <br>
     * Delete object yang sama didalam list.
     *
     * @param listAdd
     */
    public void add(List<? extends T> listAdd) {
        addList = true;
        for (int i = 0; i < listAdd.size(); i++) {
            add(listAdd.get(i));
        }
        notifyDataSetChanged();
        addList = false;
    }

    public void add(T entity) {
        int index = indexOf(entity);
        if (index != -1) {
            list.add(index + 1, entity);
            list.remove(index + 1);
        } else {
            list.add(entity);
        }
        if (!addList) {
            notifyDataSetChanged();
        }
    }

    public void remove(T entity) {
        list.remove(entity);
        if (!addList) {
            notifyDataSetChanged();
        }
    }

    /**
     * tambah ke array list parent. sebelumnya Object T harus override function
     * equals() yang sudah disediakan oleh java. <br>
     * Delete object yang sama didalam list.
     *
     * @param listRemove
     */
    public void remove(List<? extends T> listRemove) {
        addList = true;
        for (int i = 0; i < listRemove.size(); i++) {
            remove(listRemove.get(i));
        }
        notifyDataSetChanged();
        addList = false;
    }

    public boolean contains(T entity) {
        return list.contains(entity);
    }

    /**
     * Searches this {@code List} for the specified object and returns the index
     * of the first occurrence.
     *
     * @param entity the object to search for.
     * @return the index of the first occurrence of the object or -1 if the
     * object was not found.
     */
    public int indexOf(T entity) {
        return list.indexOf(entity);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    public List<T> getList() {
        return list;
    }

    /**
     * @param index
     * @return item in the list. null if index out of bound
     */
    @Override
    public T getItem(int index) {
        if (index >= list.size()) {
            return null;
        }
        return list.get(index);
    }

    @Override
    public long getItemId(int index) {
        return list.get(index).hashCode();
    }

    @Override
    public abstract View getView(int index, View convertView, ViewGroup parent);

    @Override
    public abstract void onItemClick(AdapterView<?> parent, View view, int position, long id);

}
