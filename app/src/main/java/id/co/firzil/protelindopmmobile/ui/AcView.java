package id.co.firzil.protelindopmmobile.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import id.co.firzil.protelindopmmobile.R;

/**
 * Created by Rio Rizky Rainey on 21/03/2015.
 * rizkyrainey@gmail.com
 */
public class AcView extends LinearLayout {

    private int indexAc;

    public AcView(Context context, int indexAc) {
        super(context);
        this.indexAc = indexAc;
        init();
    }

    public AcView(Context context, AttributeSet attrs, int indexAc) {
        super(context, attrs);
        this.indexAc = indexAc;
        init();
    }

    public AcView(Context context, AttributeSet attrs, int defStyleAttr, int indexAc) {
        super(context, attrs, defStyleAttr);
        this.indexAc = indexAc;
        init();
    }

    private void init() {
        setOrientation(VERTICAL);
        setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        inflate(getContext(), R.layout.form_ac, this);
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            if (child instanceof TextView && !(child instanceof EditText)) {
                TextView childTextView = (TextView) child;
                String firstText = childTextView.getText().toString();
                childTextView.setText(firstText + "#" + indexAc);
            }
        }
    }

    public int getIndexAc() {
        return indexAc;
    }

    public void setIndexAc(int indexAc) {
        this.indexAc = indexAc;
    }
}
