package id.co.firzil.protelindopmmobile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.gz.databaselibrary.GzDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import id.co.firzil.protelindopmmobile.service.SubmitAnyarService;

/**
 * Created by Rio Rizky Rainey on 13/02/2015.
 * rizkyrainey@gmail.com
 */
public class Me {

    public static final String IDVENDOR_USER = "idvendor_user";
    public static final String IDVENDOR = "idvendor";
    public static final String USERNAME = "username";
    public static final String FULLNAME = "fullname";
    public static final String AVATAR = "avatar";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";
    public static final String IMEI_NUMBER = "imei_number";
    public static final String MOBILE_APP_ACCESS = "mobile_app_access";
    public static final String OM_CREATE_DATE = "om_create_date";
    public static final String OM_CREATE_BY = "om_create_by";
    public static final String OM_UPDATE_DATE = "om_update_date";
    public static final String OM_UPDATE_BY = "om_update_by";
    public static final String OM_DELETION_FLAG = "om_deletion_flag";
    public static final String FCM_REGID = "fcm_regid";

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences("xyz", Context.MODE_PRIVATE);
    }

    public static void commit(Context context, String key, String value) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void commitFromJSONObj(Context context, JSONObject jsonObject, JSONObject keseluruhan) {
        Me.commit(context, Me.IDVENDOR_USER, jsonObject.optString(Me.IDVENDOR_USER));
        Me.commit(context, Me.IDVENDOR, jsonObject.optString(Me.IDVENDOR));
        Me.commit(context, Me.USERNAME, jsonObject.optString(Me.USERNAME));
        Me.commit(context, Me.IDVENDOR, jsonObject.optString(Me.IDVENDOR));
        Me.commit(context, Me.FULLNAME, jsonObject.optString(Me.FULLNAME));
        Me.commit(context, Me.EMAIL, jsonObject.optString(Me.EMAIL));
        Me.commit(context, Me.PHONE, jsonObject.optString(Me.PHONE));
        Me.commit(context, Me.IMEI_NUMBER, jsonObject.optString(Me.IMEI_NUMBER));
        Me.commit(context, Me.MOBILE_APP_ACCESS, jsonObject.optString(Me.MOBILE_APP_ACCESS));
        Me.commit(context, Me.OM_CREATE_DATE, jsonObject.optString(Me.OM_CREATE_DATE));
        Me.commit(context, Me.OM_CREATE_BY, jsonObject.optString(Me.OM_CREATE_BY));
        Me.commit(context, Me.OM_UPDATE_DATE, jsonObject.optString(Me.OM_UPDATE_DATE));
        Me.commit(context, Me.OM_UPDATE_BY, jsonObject.optString(Me.OM_UPDATE_BY));
        Me.commit(context, Me.OM_DELETION_FLAG, jsonObject.optString(Me.OM_DELETION_FLAG));
        Me.commit(context, Me.AVATAR, jsonObject.optString(Me.AVATAR));

        Me.commit(context, "radius_tolerance", "200");
        Me.commit(context, "min_distance_update_gps", "1");
        Me.commit(context, "min_time_update_gps", "3000");

        try {
            JSONArray jsonArray = keseluruhan.getJSONArray("config");
            for(int i=0;i<jsonArray.length();i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                String configName = object.getString("config_name");
                String value = object.getString("config_value");

                Me.commit(context, configName, value);
                System.out.println(configName + " " + value);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static String getValue(Context context, String key) {
        return getSharedPreferences(context).getString(key, null);
    }

    public static void logout(Context context) {
        System.out.println("LOGOUT");
        context.stopService(new Intent(context, SubmitAnyarService.class));

        getSharedPreferences(context).edit().clear().commit();
        GzDatabase.getInstance().deleteAllData();
        Utils.deleteRecursive(new File(Constants.FILE_IMAGE));
        TimeDetector.getInstance().resetGpsInfo();
    }

    public static boolean isLogin(Context context) {
        return getSharedPreferences(context).contains(Me.EMAIL);
    }

}
