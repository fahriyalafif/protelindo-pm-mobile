package id.co.firzil.protelindopmmobile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.support.v4.content.LocalBroadcastManager;

public class GpsLocationReceiver extends BroadcastReceiver {        
    @Override
    public void onReceive(Context c, Intent in) {
        if (in.getAction().equals(LocationManager.PROVIDERS_CHANGED_ACTION)) {
        	LocalBroadcastManager.getInstance(c).sendBroadcast(in);
        }
    }
}
