package id.co.firzil.protelindopmmobile.entity;

import android.database.Cursor;

import com.gz.databaselibrary.GzDatabase;
import com.gz.databaselibrary.GzEntity;
import com.gz.databaselibrary.Utils;
import com.gz.databaselibrary.annotation.Column;
import com.gz.databaselibrary.annotation.Id;
import com.gz.databaselibrary.annotation.Table;

import java.util.ArrayList;
import java.util.List;

import static com.gz.databaselibrary.GzApplication.getInstance;

/**
 * Created by Rio Rizky Rainey on 20/03/2015.
 * rizkyrainey@gmail.com
 */
@Table(name = "tenant")
public class Tenant extends GzEntity {

    @Id
    @Column(name = "id_tenant")
    private Integer idTenant;

    @Column(name = "SiteId")
    private String idSite;

    @Column(name = "ClientId")
    private String idClient;

    @Column(name = "RFIDate")
    private String rfiDate;

    @Column(name = "EquipmentType")
    private Integer equipmentType;

    @Column(name = "SourcePower")
    private String sourcePower;

    @Column(name = "Maintainance")
    private Integer maintenance;

    public Tenant() {
    }

    public Tenant(Integer idTenant, String idSite, String idClient, String rfiDate, Integer equipmentType, String sourcePower, Integer maintenance) {
        this.idTenant = idTenant;
        this.idSite = idSite;
        this.idClient = idClient;
        this.rfiDate = rfiDate;
        this.equipmentType = equipmentType;
        this.sourcePower = sourcePower;
        this.maintenance = maintenance;
    }

    public static List<Tenant> findBySiteId(String idSite) {
        String tableName = Utils.getTableAnnotation(Tenant.class).name();
        String sql = "SELECT * FROM " + tableName + " WHERE SiteId=?";
        Cursor cursor = getInstance().getOpenHelper().getDatabase().rawQuery(sql, new String[]{idSite});
        List<Tenant> list = new ArrayList<Tenant>();
        try {
            while (cursor.moveToNext()) {
                @SuppressWarnings("unchecked")
                Tenant entity = (Tenant) Utils.setEntityFromCursor(Tenant.class, cursor);
                list.add(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return list;
    }

    public static int countTenant(String idSite) {
        String tableName = Utils.getTableAnnotation(Tenant.class).name();
        String sql = "SELECT * FROM " + tableName + " WHERE SiteId=?";
        Cursor cursor = getInstance().getOpenHelper().getDatabase().rawQuery(sql, new String[]{idSite});
        int count = 0;
        while (cursor.moveToNext()) {
            count = cursor.getInt(0);
        }
        cursor.close();
        return count;
    }

    public static boolean isHasData(String idSite) {
        String tableName = Utils.getTableAnnotation(Tenant.class).name();
        String sql = "SELECT COUNT(*) FROM " + tableName + " WHERE SiteId=?";
        Cursor cursor = GzDatabase.getInstance().getDatabase().rawQuery(sql, new String[]{idSite});
        int count = 0;
        while (cursor.moveToNext()) {
            count = cursor.getInt(0);
        }
        cursor.close();
        return count > 0;
    }

    public Integer getIdTenant() {
        return idTenant;
    }

    public void setIdTenant(Integer idTenant) {
        this.idTenant = idTenant;
    }

    public String getIdSite() {
        return idSite;
    }

    public void setIdSite(String idSite) {
        this.idSite = idSite;
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getRfiDate() {
        return rfiDate;
    }

    public void setRfiDate(String rfiDate) {
        this.rfiDate = rfiDate;
    }

    public Integer getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(Integer equipmentType) {
        this.equipmentType = equipmentType;
    }

    public String getSourcePower() {
        return sourcePower;
    }

    public void setSourcePower(String sourcePower) {
        this.sourcePower = sourcePower;
    }

    public Integer getMaintenance() {
        return maintenance;
    }

    public void setMaintenance(Integer maintenance) {
        this.maintenance = maintenance;
    }

    @Override
    public void saveMerge() {
        if (isExist(getClass(), "SiteId, ClientId", getIdSite(), getIdClient())) {
            updateBy("SiteId=? AND ClientId=?", new String[]{"" + getIdSite(), "" + getIdClient()});
        } else {
            save();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tenant tenant = (Tenant) o;

        if (!idClient.equals(tenant.idClient)) return false;
        if (!idSite.equals(tenant.idSite)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idSite.hashCode();
        result = 31 * result + idClient.hashCode();
        return result;
    }
}
