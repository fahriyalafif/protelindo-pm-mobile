package id.co.firzil.protelindopmmobile;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.gz.databaselibrary.GzDatabase;

/**
 * Created by fahriyalafif on 3/10/2016.
 */
public class DB extends GzDatabase {

    private static DB iki;
    private static SQLiteDatabase db;

    public DB(Context c) {
        super(c);
    }

    public static DB get(Context c){
        if(iki == null){
            iki = new DB(c);
            db = iki.getWritableDatabase();
        }
        return iki;
    }

    public void saveAndMergeStatusHistory(String idpm_history, String idpm, String date, String status, String remark, String vendor){
        int count = database.query("status_history", null, "idpm_history=?", new String[]{idpm_history}, null, null, null).getCount();

        ContentValues v = new ContentValues();
        v.put("idpm_history", idpm_history);
        v.put("idpm", idpm);
        v.put("pmh_status", status);
        v.put("pmh_date", date);
        v.put("pmp_vendor", vendor);
        v.put("pmh_remark", remark);

        if(count > 0){
            db.update("status_history", v, "idpm_history=?", new String[]{idpm_history});
        }
        else{
            db.insert("status_history", "", v);
        }
    }

}
