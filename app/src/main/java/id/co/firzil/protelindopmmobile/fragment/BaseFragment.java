package id.co.firzil.protelindopmmobile.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gz.databaselibrary.ServiceHandler;

import java.util.ArrayList;
import java.util.List;

import id.co.firzil.protelindopmmobile.OnBackPressedListener;

/**
 * Created by Rio Rizky Rainey on 13/02/2015.
 * rizkyrainey@gmail.com
 */
public abstract class BaseFragment extends Fragment implements OnBackPressedListener {

    private ViewGroup rootView;
    private LayoutInflater inflater;
    private ViewGroup container;
    protected Bundle savedInstanceState;

    private List<ServiceHandler> handlerList;

    public BaseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handlerList = new ArrayList<>();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.inflater = inflater;
        this.container = container;
        this.savedInstanceState = savedInstanceState;
        createView();
        return rootView;
    }

    public abstract void createView();

    public abstract String getTagFragment();

    public ViewGroup getRootView() {
        return rootView;
    }

    public View findViewById(int idView) {
        return rootView.findViewById(idView);
    }

    public LayoutInflater getInflater() {
        return inflater;
    }

    public ViewGroup getContainer() {
        return container;
    }

    public Bundle getSavedInstanceState() {
        return savedInstanceState;
    }

    public void setContentView(int idLayout) {
        this.rootView = (ViewGroup) inflater.inflate(idLayout, container, false);
    }

    public void addServiceHandler(ServiceHandler serviceHandler) {
        handlerList.add(serviceHandler);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for (ServiceHandler serviceHandler : handlerList) {
            serviceHandler.destroyAsync();
        }
    }

}
