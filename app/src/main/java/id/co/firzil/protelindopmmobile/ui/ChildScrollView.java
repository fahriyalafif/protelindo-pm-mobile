package id.co.firzil.protelindopmmobile.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

/**
 * Created by Rio Rizky Rainey on 03/03/2015.
 * rizkyrainey@gmail.com
 */
public class ChildScrollView extends ScrollView {
    public ChildScrollView(Context context) {
        super(context);
        smoothScrollBy(0, 100);
    }

    public ChildScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        smoothScrollBy(0, 100);
    }

    public ChildScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        smoothScrollBy(0, 100);
    }

}
