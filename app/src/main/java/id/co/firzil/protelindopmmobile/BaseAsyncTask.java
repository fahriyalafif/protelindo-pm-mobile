package id.co.firzil.protelindopmmobile;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

/**
 * Created by Rio Rizky Rainey on 24/02/2015.
 * rizkyrainey@gmail.com
 */
public abstract class BaseAsyncTask<Params, Progress, Result> extends AsyncTask {

    protected OnTaskCompleted onTaskCompleted;
    private Context context;
    private ProgressDialog progressDialog;

    public BaseAsyncTask(Context context, String message) {
        this.context = context;
        this.progressDialog = new ProgressDialog(context);
        this.progressDialog.setMessage(message);
        this.progressDialog.setCancelable(false);
    }

    protected BaseAsyncTask(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public void setOnTaskCompleted(OnTaskCompleted onTaskCompleted) {
        this.onTaskCompleted = onTaskCompleted;
    }

    @Override
    protected void onPreExecute() {
        if(progressDialog!=null)
            progressDialog.show();
    }

    @Override
    protected void onPostExecute(Object result) {
        if(progressDialog!=null)
            progressDialog.dismiss();
        if(onTaskCompleted!=null)
            onTaskCompleted.onTaskCompleted(result);
    }

}
