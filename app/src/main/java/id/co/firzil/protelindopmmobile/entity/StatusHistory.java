package id.co.firzil.protelindopmmobile.entity;

import android.database.Cursor;

import com.gz.databaselibrary.GzDatabase;
import com.gz.databaselibrary.GzEntity;
import com.gz.databaselibrary.Utils;
import com.gz.databaselibrary.annotation.Column;
import com.gz.databaselibrary.annotation.Id;
import com.gz.databaselibrary.annotation.Table;

import java.util.ArrayList;
import java.util.List;

import static com.gz.databaselibrary.GzApplication.getInstance;

/**
 * Created by Rio Rizky Rainey on 16/03/2015.
 * rizkyrainey@gmail.com
 */
@Table(name = "status_history")
public class StatusHistory extends GzEntity {

    @Id
    @Column(name = "idpm_history")
    private Integer idStatusHistory;

    @Column(name = "idpm")
    private Integer idPm;

    @Column(name = "pmh_status")
    private String status;

    @Column(name = "pmh_date")
    private String date;

    @Column(name = "pmp_vendor")
    private String assign;

    @Column(name = "pmh_remark")
    private String description;

    public StatusHistory() {
    }

    public static List<StatusHistory> findByIdPm(String idPm) {
        String tableName = StatusHistory.class.getAnnotation(Table.class).name();
        String sql = "SELECT * FROM " + tableName + " WHERE idpm=?";
        Cursor cursor = getInstance().getOpenHelper().getDatabase().rawQuery(sql, new String[]{idPm});
        List<StatusHistory> list = new ArrayList<>();
        try {
            while (cursor.moveToNext()) {
                @SuppressWarnings("unchecked")
                StatusHistory entity = Utils.setEntityFromCursor(StatusHistory.class, cursor);
                list.add(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return list;
    }

    public static boolean isHasData(String idPm) {
        String tableName = Utils.getTableAnnotation(StatusHistory.class).name();
        String sql = "SELECT COUNT(*) FROM " + tableName + " WHERE idpm=?";
        Cursor cursor = GzDatabase.getInstance().getDatabase().rawQuery(sql, new String[]{idPm});
        int count = 0;
        while (cursor.moveToNext()) {
            count = cursor.getInt(0);
        }
        cursor.close();
        return count > 0;
    }

    public void setIdPm(Integer idPm) {
        this.idPm = idPm;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAssign() {
        return assign;
    }

    public void setAssign(String assign) {
        this.assign = assign;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setIdStatusHistory(Integer idStatusHistory){
        this.idStatusHistory = idStatusHistory;
    }

    public Integer getIdStatusHistory(){
        return idStatusHistory;
    }

    public Integer getIdPm(){
        return idPm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StatusHistory that = (StatusHistory) o;

        return idStatusHistory.equals(that.idStatusHistory);
    }

    @Override
    public int hashCode() {
        return idStatusHistory.hashCode();
    }
}
