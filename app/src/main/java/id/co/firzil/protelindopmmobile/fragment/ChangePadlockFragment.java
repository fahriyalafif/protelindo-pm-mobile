package id.co.firzil.protelindopmmobile.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gz.databaselibrary.ConnectionListener;
import com.gz.databaselibrary.ServiceHandler;

import org.json.JSONException;
import org.json.JSONObject;

import id.co.firzil.protelindopmmobile.Me;
import id.co.firzil.protelindopmmobile.R;
import id.co.firzil.protelindopmmobile.Tag;
import id.co.firzil.protelindopmmobile.URL;
import id.co.firzil.protelindopmmobile.Utils;
import id.co.firzil.protelindopmmobile.entity.TaskList;

/**
 * Created by Rio Rizky Rainey on 03/03/2015.
 * rizkyrainey@gmail.com
 */
public class ChangePadlockFragment extends BaseActivity {

    private EditText padlockEditText;
    private TextView padlockTextView;

    private TaskList taskList;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        taskList = (TaskList) getIntent().getExtras().getSerializable("taskList");
        String padlock = null;

        if(taskList != null) padlock = taskList.getNumberPadlock();

        setContentView(R.layout.fragment_changepadlock);
        setNavbar();
        padlockEditText = (EditText) findViewById(R.id.editText_padlock);
        padlockTextView = (TextView) findViewById(R.id.textView_padlock);

        padlockTextView.setText("Current Padlock : "+ padlock);

        Button saveButton = (Button) findViewById(R.id.update);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newPadlock = padlockEditText.getText().toString();
                ServiceHandler serviceHandler = new ServiceHandler(ChangePadlockFragment.this, getString(R.string.tunggu));
                serviceHandler.addParam(Tag.IDUSER, Me.getValue(ChangePadlockFragment.this, Me.IDVENDOR_USER));
                serviceHandler.addParam(Tag.IDSITE, taskList.getIdSite());
                serviceHandler.addParam(Tag.IDTASK, "" + taskList.getIdPmPlanDetil());
                serviceHandler.addParam(Tag.NEW_PADLOCK, newPadlock);
                serviceHandler.setConnectionListener(new ConnectionListener() {
                    @Override
                    public void onBeforeRequest() {
                    }

                    @Override
                    public void onAfterRequested(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String msg = jsonObject.getString(Tag.MSG);
                            if (!Utils.isTextEmpty(msg))
                                Utils.makeToast(ChangePadlockFragment.this, msg);
                            if (jsonObject.optInt(Tag.FLAG) == 1) {
                                Utils.makeToast(ChangePadlockFragment.this, "Padlock sudah disimpan");
                                String padlock_baru = jsonObject.getString(Tag.NEW_PADLOCK);
                                padlockTextView.setText("Current Padlock : " + padlock_baru);
                                taskList.setNumberPadlock(padlock_baru);
                                taskList.saveMerge();

                                Intent i = new Intent();
                                i.putExtra("padlock_baru", padlock_baru);

                                setResult(RESULT_OK, i);
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                serviceHandler.makeServiceCallAsyncTask(URL.CHANGE_PADLOCK, ServiceHandler.POST);
//                }
            }
        });
    }

}
