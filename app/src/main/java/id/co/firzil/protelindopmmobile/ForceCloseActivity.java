package id.co.firzil.protelindopmmobile;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by riorizkyrainey on 09/09/15.
 */
public class ForceCloseActivity extends Activity {

    TextView error;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_force_close);

        error = (TextView) findViewById(R.id.error);

        error.setText(getIntent().getStringExtra("error"));
    }
}
