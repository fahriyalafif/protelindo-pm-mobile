package id.co.firzil.protelindopmmobile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import id.co.firzil.protelindopmmobile.service.SubmitAnyarService;

/**
 * Created by Rio Rizky Rainey on 30/03/2015.
 * rizkyrainey@gmail.com
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context c, Intent intent) {
        Intent intent1 = new Intent(c, SubmitAnyarService.class);
        if (Utils.isOnline(c)) c.startService(intent1);
        else c.stopService(intent1);
    }

}
