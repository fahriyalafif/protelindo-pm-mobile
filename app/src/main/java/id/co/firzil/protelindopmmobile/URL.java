package id.co.firzil.protelindopmmobile;

/**
 * Created by Rio Rizky Rainey on 27/02/2015.
 * rizkyrainey@gmail.com
 */
public class URL {

    //Do not edit unless you know what you are doing
    public static String URL_SERVER = "https://pmcm.protelindo.net:6443/";
    public static String BASE_URL = URL_SERVER + "index.php/rest/";
    public static String LOGIN = BASE_URL + "user/login";
    public static String FORGET_PASSWORD = BASE_URL + "user/forget_password";
    public static String UPDATE_PROFILE = BASE_URL + "user/update_profile";
    public static String TASK_LIST_OVERVIEW = BASE_URL + "task/task_list_overview";
    public static String MY_TASK_LIST = BASE_URL + "task/my_task_list";
    public static String SAVE_META = BASE_URL + "meta/save";
    public static String TENANT_LIST = BASE_URL + "task/tenant_list";
    public static String CHANGE_PADLOCK = BASE_URL + "padlock/change";
    public static String PADLOCK_COMBINATION = BASE_URL + "padlock/combination";
    public static String SUBMIT_REPORT = BASE_URL + "task/submit_report";
    public static String GET_ALL_META = BASE_URL + "meta/all_meta";
    public static String TASK_HISTORY = BASE_URL + "task/task_history";
    public static String BLOCKED_ACCESS = BASE_URL + "task/set_blocked_access";
    public static String URL_IMAGE = URL_SERVER + "asset/user-image/";
    public static String URL_IMAGE_META = URL_SERVER + "";
    public static String URL_VERSION_CHECKER = BASE_URL + "app/version_checker";
    public static String URL_LOG_LATLONG = BASE_URL + "log_lat_long/save";
    public static String UPDATE_REGID = BASE_URL + "user/update_regid";

    public static void changeAllURL() {
        BASE_URL = URL_SERVER + "index.php/rest/";
        LOGIN = BASE_URL + "user/login";
        FORGET_PASSWORD = BASE_URL + "user/forget_password";
        UPDATE_PROFILE = BASE_URL + "user/update_profile";
        TASK_LIST_OVERVIEW = BASE_URL + "task/task_list_overview";
        MY_TASK_LIST = BASE_URL + "task/my_task_list/";
        SAVE_META = BASE_URL + "meta/save";
        TENANT_LIST = BASE_URL + "task/tenant_list";
        CHANGE_PADLOCK = BASE_URL + "padlock/change";
        PADLOCK_COMBINATION = BASE_URL + "padlock/combination";
        SUBMIT_REPORT = BASE_URL + "task/submit_report";
        GET_ALL_META = BASE_URL + "meta/all_meta";
        TASK_HISTORY = BASE_URL + "task/task_history";
        BLOCKED_ACCESS = BASE_URL + "task/set_blocked_access";
        URL_IMAGE = URL_SERVER + "asset/user-image/";
        URL_IMAGE_META = URL_SERVER + "";
        URL_VERSION_CHECKER = BASE_URL + "app/version_checker";
        URL_LOG_LATLONG = BASE_URL + "log_lat_long/save";
        UPDATE_REGID = BASE_URL + "user/update_regid";
    }
}
