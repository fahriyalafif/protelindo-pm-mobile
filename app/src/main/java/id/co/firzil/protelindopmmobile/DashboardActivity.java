package id.co.firzil.protelindopmmobile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.gz.databaselibrary.ConnectionListener;
import com.gz.databaselibrary.ServiceHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.co.firzil.protelindopmmobile.entity.TaskListOverview;
import id.co.firzil.protelindopmmobile.fragment.BaseActivity;
import id.co.firzil.protelindopmmobile.fragment.BaseFragment;
import id.co.firzil.protelindopmmobile.fragment.NavigationDrawerFragment;
import id.co.firzil.protelindopmmobile.fragment.PMListFragment;

public class DashboardActivity extends BaseActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks{
    private static final String TAG = "Dashboard";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View roote = getLayoutInflater().inflate(R.layout.activity_dashboard, null);
        setContentView(roote);
        setNavbar();

        BaseFragment baseFragment = new DashboardFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
                .replace(R.id.container, baseFragment, baseFragment.getTagFragment()).commit();

        ((TextView) roote.findViewById(R.id.title)).setText(TAG);

        checkPlayServices();
    }

    private void checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, 9000).show();
            }
            else {
                Utils.makeToast(c, "Please update your Google Play Services");
                finish();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        TimeDetector.getInstance().resetGpsInfo();
        TimeDetector.getInstance().stop();
    }

    @Override
    public void setTitle(String title) {
        ((TextView) findViewById(R.id.title)).setText(title);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class DashboardFragment extends BaseFragment {

        private ExpandableListView listDashboard;
        private ExpendableDashboardAdapter expendableAdapter;
        private List<TaskListOverview> listOverviews, listOverviewsKemarin;

        public DashboardFragment() {
        }

        @Override
        public void createView() {
            setContentView(R.layout.fragment_dashboard);
//            adapter = new DashboardAdapter(getActivity(), getActivity().getSupportFragmentManager());
            expendableAdapter = new ExpendableDashboardAdapter(getActivity());
            listDashboard = (ExpandableListView) findViewById(R.id.list_dashboard);
            listDashboard.setAdapter(expendableAdapter);
//            listDashboard.setOnItemClickListener(adapter);
            listDashboard.setOnChildClickListener(expendableAdapter);
            loadTaskOverview(false);
        }

        @Override
        public String getTagFragment() {
            return TAG;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            System.out.println("ONOPTIONS SELECTED Dashboard" + item.getItemId());
            switch (item.getItemId()) {
                case R.id.main:
                    loadTaskOverview(true);
                    break;
            }
            return super.onOptionsItemSelected(item);
        }

        private void loadTaskOverview(boolean refresh) {
            expendableAdapter = new ExpendableDashboardAdapter(getActivity());
            listDashboard.setAdapter(expendableAdapter);
            listDashboard.setOnChildClickListener(expendableAdapter);

            if(refresh && Utils.isOnline(getActivity())){
                TaskListOverview.kosongkanByPeriode(TaskListOverview.PERIODE_SEKARANG);
                TaskListOverview.kosongkanByPeriode(TaskListOverview.PERIODE_KEMARIN);
            }

            listOverviews = TaskListOverview.getListByPeriode(TaskListOverview.PERIODE_SEKARANG);
            listOverviewsKemarin = TaskListOverview.getListByPeriode(TaskListOverview.PERIODE_KEMARIN);
            expendableAdapter.add(1, listOverviews);
            expendableAdapter.add(0, listOverviewsKemarin);
            expendableAdapter.removeCountZero();

            if (Utils.isOnline(getActivity())) {
                ServiceHandler serviceHandler;
                if (!TaskListOverview.isHasData(TaskListOverview.class) || refresh) {
                    serviceHandler = new ServiceHandler(getActivity(), getString(R.string.tunggu));
                } else {
                    serviceHandler = new ServiceHandler(getActivity(), false);
                }
                serviceHandler.addParam(Tag.IDUSER, /*"" + 3*/  Me.getValue(getActivity(), Me.IDVENDOR_USER));
                serviceHandler.addParam(Tag.PENDING_REVIEW, "0");
                serviceHandler.setConnectionListener(new ConnectionListener() {
                    @Override
                    public void onBeforeRequest() {
                    }

                    @Override
                    public void onAfterRequested(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optInt(Tag.FLAG) == 1) {
                                JSONArray jsonArray = jsonObject.getJSONArray(Tag.RESULT);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    TaskListOverview overview = new TaskListOverview();
                                    overview.initFromJSONObject(jsonArray.getJSONObject(i));
                                    overview.saveMerge(); //save ke db ketika tidak ada, update ketika sudah ada.
                                    listOverviews.add(overview);
                                    expendableAdapter.add(1, listOverviews);
                                    expendableAdapter.removeCountZero();
                                }
                            }

                            ServiceHandler serviceHandlerPendingReview;
                            if (TaskListOverview.isHasData(TaskListOverview.class)) {
                                serviceHandlerPendingReview = new ServiceHandler(getActivity(), false);
                            } else {
                                serviceHandlerPendingReview = new ServiceHandler(getActivity(), getString(R.string.tunggu));
                            }
                            serviceHandlerPendingReview.addParam(Tag.IDUSER, /*"" + 3*/  Me.getValue(getActivity(), Me.IDVENDOR_USER));
                            serviceHandlerPendingReview.addParam(Tag.PENDING_REVIEW, "1");
                            serviceHandlerPendingReview.setConnectionListener(new ConnectionListener() {
                                @Override
                                public void onBeforeRequest() {
                                }

                                @Override
                                public void onAfterRequested(String response) {
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        if (jsonObject.optInt(Tag.FLAG) == 1) {
                                            JSONArray jsonArray = jsonObject.getJSONArray(Tag.RESULT);
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                TaskListOverview overview = new TaskListOverview();
                                                overview.initFromJSONObject(jsonArray.getJSONObject(i));
                                                overview.setPeriode(1); //set periode kemarin
                                                overview.saveMerge(); //save ke db ketika tidak ada, update ketika sudah ada.
                                                listOverviewsKemarin.add(overview);
                                                expendableAdapter.add(0, listOverviewsKemarin);
                                                expendableAdapter.removeCountZero();
                                            }
                                        }
                                    } catch (JSONException e) {
                                        Utils.makeToast(getActivity(), "Tidak dapat terhubung ke jaringan");
                                    }
                                }
                            });
                            addServiceHandler(serviceHandlerPendingReview);
                            serviceHandlerPendingReview.makeServiceCallAsyncTask(URL.TASK_LIST_OVERVIEW, ServiceHandler.GET);

                        } catch (JSONException e) {
                            Utils.makeToast(getActivity(), "Tidak dapat terhubung ke jaringan");
                        }
                    }
                });
                addServiceHandler(serviceHandler);
                serviceHandler.makeServiceCallAsyncTask(URL.TASK_LIST_OVERVIEW, ServiceHandler.GET);

            }
        }

    }


    public static class ExpendableDashboardAdapter extends BaseExpandableListAdapter implements ExpandableListView.OnChildClickListener {

        private Context context;
        private LayoutInflater inflater;
        private List<TaskListOverview> periodeKemarin;
        private List<TaskListOverview> periodeSekarang;
        private boolean addList;

        public ExpendableDashboardAdapter(Context context) {
            this.context = context;
            inflater = LayoutInflater.from(context);
            periodeKemarin = new ArrayList<>();
            periodeSekarang = new ArrayList<>();
        }

        /**
         * tambah ke array list parent. sebelumnya Object T harus override function
         * equals() yang sudah disediakan oleh java. <br>
         * Delete object yang sama didalam list.
         *
         * @param groupPosition
         * @param listAdd
         */
        public void add(int groupPosition, List<TaskListOverview> listAdd) {
            addList = true;

            for (int i = 0; i < listAdd.size(); i++) {
                add(groupPosition, listAdd.get(i));
            }

            notifyDataSetChanged();
            addList = false;
        }

        public void add(int groupPosition, TaskListOverview entity) {
            List<TaskListOverview> list;
            if (groupPosition == 0) list = periodeKemarin;
            else list = periodeSekarang;

            int index = list.indexOf(entity);
            if (index != -1) {
                list.add(index + 1, entity);
                list.remove(index + 1);
            } else {
                list.add(entity);
            }
            if (!addList) {
                notifyDataSetChanged();
            }
        }

        public void removeCountZero() {
            for (int j = 0; j < getGroupCount(); j++) {
                List<TaskListOverview> removeList = new ArrayList<>();
                for (int i = 0; i < getChildrenCount(j); i++) {
                    TaskListOverview listOverview = getChild(j, i);
                    if (listOverview.getCount() == 0) removeList.add(listOverview);
                }
                remove(j, removeList);
            }
            notifyDataSetChanged();
        }

        public boolean remove(int groupPosition, List<TaskListOverview> removeList) {
            if (groupPosition == 0) return periodeKemarin.removeAll(removeList);
            else if (groupPosition == 1) return periodeSekarang.removeAll(removeList);
            return false;
        }

        @Override
        public int getGroupCount() {
            return 2;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            if (groupPosition == 0) return periodeKemarin.size();
            else if (groupPosition == 1) return periodeSekarang.size();
            return 0;
        }

        @Override
        public String getGroup(int groupPosition) {
            int size = 0;
            if (groupPosition == 0) {
                for (int i = 0; i < periodeKemarin.size(); i++) {
                    size += getChild(groupPosition, i).getCount();
                }
                return "Periode Kemarin - " + size;
            } else if (groupPosition == 1) {
                for (int i = 0; i < periodeSekarang.size(); i++) {
                    size += getChild(groupPosition, i).getCount();
                }
                return "Periode Sekarang - " + size;
            }
            return null;
        }

        @Override
        public TaskListOverview getChild(int groupPosition, int childPosition) {
            if (groupPosition == 0) return periodeKemarin.get(childPosition);
            else if (groupPosition == 1) return periodeSekarang.get(childPosition);
            return null;
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            TextView textView = (TextView) inflater.inflate(R.layout.list_group_dashboard, parent, false);
            textView.setText(getGroup(groupPosition));
            convertView = textView;
            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            convertView = inflater.inflate(R.layout.list_child_dashboard, parent, false);
            Integer count = getChild(groupPosition, childPosition).getCount();
            String pmpStatus = getChild(groupPosition, childPosition).getPmpStatus();
            ((TextView) convertView).setText(pmpStatus + " - " + count);

            int idColor = 0;
            if ("Remaining PM task list".equalsIgnoreCase(pmpStatus)) {
                idColor = R.color.color_green_approved;
            } else if ("Approve by Protelindo".equalsIgnoreCase(pmpStatus)) {
                idColor = R.color.color_green_approved;
            } else if ("Rejected by Protelindo".equalsIgnoreCase(pmpStatus)) {
                idColor = R.color.color_red_rejected;
            } else if ("Approve by Vendor".equalsIgnoreCase(pmpStatus)) {
                idColor = R.color.color_green_approved;
            } else if ("Rejected by Vendor".equalsIgnoreCase(pmpStatus)) {
                idColor = R.color.color_red_rejected;
            } else if ("Blocked Access".equalsIgnoreCase(pmpStatus)) {
                idColor = R.color.color_yellow_blocked;
            } else {
                idColor = R.color.color_blue;
            }
            convertView.setBackgroundColor(context.getResources().getColor(idColor));
            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        @Override
        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
            Intent intent = new Intent(context, PMListFragment.class);
            intent.putExtra("pmstatus", getChild(groupPosition, childPosition).getPmpStatus());
            intent.putExtra("periode", groupPosition == 1 ? 0 : 1);
            context.startActivity(intent);
            return true;
        }
    }
}
