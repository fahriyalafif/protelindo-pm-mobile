package id.co.firzil.protelindopmmobile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.firebase.iid.FirebaseInstanceId;
import com.gz.databaselibrary.ConnectionListener;
import com.gz.databaselibrary.ServiceHandler;

import org.json.JSONException;
import org.json.JSONObject;

import id.co.firzil.protelindopmmobile.ui.WarningTextView;

public class LoginActivity extends AppCompatActivity {

    private Context context;

    private LinearLayout parent;
    private WarningTextView warningTextView;
    private EditText usernameEditText;
    private EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));
        setContentView(R.layout.activity_login);
        context = this;
        parent = (LinearLayout) findViewById(R.id.parent);
        usernameEditText = (EditText) findViewById(R.id.editText_username);
        passwordEditText = (EditText) findViewById(R.id.editText_password);
        warningTextView = new WarningTextView(LoginActivity.this);
        findViewById(R.id.button_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickLogin();
            }
        });
        findViewById(R.id.button_lupa_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickForgotPassword();
            }
        });

        String regid = FirebaseInstanceId.getInstance().getToken();
        if(regid == null) regid = "";
        System.out.println("login token: " + regid);

        new FcmPreference(this).setIsRegisteredInServer(false);
        Me.commit(this, Me.FCM_REGID, regid);

    }

    private void onClickLogin() {
        String username = usernameEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        if (Utils.isTextEmpty(username, password)) {
            Utils.makeToast(getBaseContext(), "Username atau password tidak boleh kosong");
        } else {
            if (Utils.isOnline(context)) {
                ServiceHandler serviceHandler = new ServiceHandler(this, getResources().getString(R.string.tunggu));
                serviceHandler.addParam(Tag.USERNAME, username);
                serviceHandler.addParam(Tag.PASSWORD, password);
                serviceHandler.addParam(Tag.AKSES, "pm");
                serviceHandler.addParam(Tag.IMEI, Utils.getImei(getBaseContext()));
                serviceHandler.addParam(Tag.FLAG_IMEI, "1");
                serviceHandler.addParam(Tag.GCM_KEY, Me.getValue(getBaseContext(), Me.FCM_REGID));
                serviceHandler.setConnectionListener(new ConnectionListener() {
                    @Override
                    public void onBeforeRequest() {
                        System.out.println("URL Server: " + URL.LOGIN);
                    }

                    @Override
                    public void onAfterRequested(String response) {
                        Log.d("Response", "" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optInt(Tag.FLAG) == 1) {
                                JSONObject user = jsonObject.getJSONObject(Tag.USER_LENGKAP);
                                Me.commitFromJSONObj(getBaseContext(), user, jsonObject);
                                new FcmPreference(context).setIsRegisteredInServer(true);
                                startActivity(new Intent(context, DashboardActivity.class));
                                finish();
                            } else if (jsonObject.optInt(Tag.FLAG) == 0) {
                                if (!warningTextView.isShown()) {
                                    warningTextView.setText(jsonObject.optString(Tag.MSG, "Invalid username and password"));
                                    warningTextView.setParent(parent);
                                    parent.addView(warningTextView, 1, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                            ViewGroup.LayoutParams.WRAP_CONTENT));
                                } else {
                                    warningTextView.setText(jsonObject.optString(Tag.MSG, ""));
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                serviceHandler.makeServiceCallAsyncTask(URL.LOGIN, ServiceHandler.POST);
            } else {
                Utils.makeToast(context, "Tidak dapat terhubung ke internet. Cek Koneksi internet anda...");
            }
        }
    }

    private void onClickForgotPassword() {
        startActivity(new Intent(this, ForgotPasswordActivity.class));
    }

}
