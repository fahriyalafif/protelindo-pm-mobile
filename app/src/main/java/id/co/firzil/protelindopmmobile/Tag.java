package id.co.firzil.protelindopmmobile;

/**
 * Created by Rio Rizky Rainey on 27/02/2015.
 * rizkyrainey@gmail.com
 */
public class Tag {

    public static final String FLAG = "flag";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";

    public static final String USER_LENGKAP = "user_lengkap";
    public static final String MSG = "msg";
    public static final String EMAIL = "email";
    public static final String IDUSER = "iduser";
    public static final String RESULT = "result";
    public static final String NAME = "name";
    public static final String PHONE = "phone";
    public static final String START = "start";
    public static final String OFFSET = "offset";
    public static final String STATUS = "status";
    public static final String IDPM = "idpm";
    public static final String JENIS = "jenis";
    public static final String META_NAME = "meta_name";
    public static final String META_VALUE = "meta_value";
    public static final String META_TYPE = "meta_type";
    public static final String DATETIME = "datetime";
    public static final String ID = "id";
    public static final String IDSITE = "idsite";
    public static final String FILE_LOCATION = "file_location";
    public static final String TIME = "time";
    public static final int REQUEST_CODE_CAMERA = 0;
    public static final String VIEW = "view";
    public static final String AKSES = "akses";
    public static final String CLIENT_PIC = "ClientPIC";
    public static final String RFI_DATE = "RFIDate";
    public static final String EQUIPMENT_TYPE = "EquipmentType";
    public static final String POWER_SOURCE = "SourcePower";
    public static final String NEW_PADLOCK = "new_padlock";
    public static final String IMEI = "imei";
    public static final String FLAG_IMEI = "flag_imei";
    public static final String GATE_ID = "GateId";
    public static final String IDTASK = "idtask";
    public static final String TYPE = "type";
    public static final String DATA_META = "data_meta";
    public static final String ID_PM_DETIL = "idpm_plan_detil";
    public static final int REQUEST_CROP_PICTURE = 3;
    public static final String TASKLIST = "tasklist";
    public static final String BLOCKED_ACCESS = "blocked_access";
    public static final String PENDING_REVIEW = "pending_review";
    public static final String KEYWORD = "keyword";
    public static final String RADIUS = "radius";
    public static final String GCM_KEY = "gcm_key";
}
