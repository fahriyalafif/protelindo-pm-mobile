package id.co.firzil.protelindopmmobile.entity;

import android.content.Context;
import android.database.Cursor;
import android.os.Build;

import com.gz.databaselibrary.GzEntity;
import com.gz.databaselibrary.annotation.Column;
import com.gz.databaselibrary.annotation.Id;
import com.gz.databaselibrary.annotation.Table;

import java.util.ArrayList;
import java.util.List;

import id.co.firzil.protelindopmmobile.Me;
import id.co.firzil.protelindopmmobile.R;
import id.co.firzil.protelindopmmobile.Utils;

import static com.gz.databaselibrary.GzApplication.getInstance;

/**
 * Created by riorizkyrainey on 09/09/15.
 */
@Table(name = "log_latlong")
public class LogLatLong extends GzEntity{

    public static int SUBMIT_NOT_YET = 0;

    public static int SUBMIT_PENDING = 1;

    public static int SUBMIT_SENDING = 2;

    public static int SUBMIT_SUBMITTED = 3;

    public static int SUBMIT_FAILED = 4;

    @Id
    @Column(name = "idlog")
    private Integer idLog;

    @Column(name = "site_id")
    private String siteId;

    @Column(name = "type")
    private String type;

    @Column(name = "ref_no")
    private String refNo;

    @Column(name = "lat_ori")
    private String latOriginal;

    @Column(name = "long_ori")
    private String longOriginal;

    @Column(name = "lat_posisi")
    private String latPosisi;

    @Column(name = "long_posisi")
    private String longPosisi;

    @Column(name = "jarak")
    private String jarak;

    @Column(name = "activity_date_time")
    private String activityDateTime;

    @Column(name = "apk_version")
    private String apkVersion;

    @Column(name = "android_version")
    private String androidVersion;

    @Column(name = "model_number")
    private String modelNumber;

    @Column(name = "kernel_version")
    private String kernelVersion;

    @Column(name = "build_number")
    private String buildNumber;

    @Column(name = "imei")
    private String imei;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "hasBeenSubmitted")
    private Integer hasBeenSubmitted;


    public LogLatLong() {
        super();
    }

    public LogLatLong(Integer idLog, String siteId, String type, String refNo, String latOriginal,
                      String longOriginal, String latPosisi, String longPosisi, String jarak,
                      String activityDateTime, String apkVersion, String androidVersion,
                      String modelNumber, String kernelVersion, String buildNumber, String imei,
                      String createdBy) {
        this.idLog = idLog;
        this.siteId = siteId;
        this.type = type;
        this.refNo = refNo;
        this.latOriginal = latOriginal;
        this.longOriginal = longOriginal;
        this.latPosisi = latPosisi;
        this.longPosisi = longPosisi;
        this.jarak = jarak;
        this.activityDateTime = activityDateTime;
        this.apkVersion = apkVersion;
        this.androidVersion = androidVersion;
        this.modelNumber = modelNumber;
        this.kernelVersion = kernelVersion;
        this.buildNumber = buildNumber;
        this.imei = imei;
        this.createdBy = createdBy;
        hasBeenSubmitted = SUBMIT_NOT_YET;
    }

    public LogLatLong(Context context, String siteId, String type, String refNo, String latOriginal,
                      String longOriginal, String latPosisi, String longPosisi, String jarak,
                      String activityDateTime) {
        this.siteId = siteId;
        this.type = type;
        this.refNo = refNo;
        this.latOriginal = latOriginal;
        this.longOriginal = longOriginal;
        this.latPosisi = latPosisi;
        this.longPosisi = longPosisi;
        this.jarak = jarak;
        this.activityDateTime = activityDateTime;
        this.apkVersion = context.getResources().getString(R.string.version);
        this.androidVersion = "" + Build.VERSION.RELEASE;
        this.modelNumber =  Build.MODEL;
        this.kernelVersion = System.getProperty("os.version", "unknown");
        this.buildNumber = Build.ID;
        this.imei = Utils.getImei(context);
        this.createdBy = Me.getValue(context, Me.FULLNAME);
        hasBeenSubmitted = SUBMIT_PENDING;
    }

    public static <T> List<T> submitPendingList() {
        String tableName = LogLatLong.class.getAnnotation(Table.class).name();
        String sql = "SELECT * FROM " + tableName + " WHERE hasBeenSubmitted=?";
        Cursor cursor = getInstance().getOpenHelper().getDatabase().rawQuery(sql, new String[]{"1"});
        List<T> list = new ArrayList<T>();
        try {
            while (cursor.moveToNext()) {
                @SuppressWarnings("unchecked")
                T entity = (T) com.gz.databaselibrary.Utils.setEntityFromCursor(LogLatLong.class, cursor);
                list.add(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return list;
    }

    public Integer getIdLog() {
        return idLog;
    }

    public void setIdLog(Integer idLog) {
        this.idLog = idLog;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getLatOriginal() {
        return latOriginal;
    }

    public void setLatOriginal(String latOriginal) {
        this.latOriginal = latOriginal;
    }

    public String getLongOriginal() {
        return longOriginal;
    }

    public void setLongOriginal(String longOriginal) {
        this.longOriginal = longOriginal;
    }

    public String getLatPosisi() {
        return latPosisi;
    }

    public void setLatPosisi(String latPosisi) {
        this.latPosisi = latPosisi;
    }

    public String getLongPosisi() {
        return longPosisi;
    }

    public void setLongPosisi(String longPosisi) {
        this.longPosisi = longPosisi;
    }

    public String getJarak() {
        return jarak;
    }

    public void setJarak(String jarak) {
        this.jarak = jarak;
    }

    public String getActivityDateTime() {
        return activityDateTime;
    }

    public void setActivityDateTime(String activityDateTime) {
        this.activityDateTime = activityDateTime;
    }

    public String getApkVersion() {
        return apkVersion;
    }

    public void setApkVersion(String apkVersion) {
        this.apkVersion = apkVersion;
    }

    public String getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(String androidVersion) {
        this.androidVersion = androidVersion;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public String getKernelVersion() {
        return kernelVersion;
    }

    public void setKernelVersion(String kernelVersion) {
        this.kernelVersion = kernelVersion;
    }

    public String getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getHasBeenSubmitted() {
        return hasBeenSubmitted;
    }

    public LogLatLong setHasBeenSubmitted(Integer hasBeenSubmitted) {
        this.hasBeenSubmitted = hasBeenSubmitted;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogLatLong that = (LogLatLong) o;

        return !(siteId != null ? !siteId.equals(that.siteId) : that.siteId != null);
    }

    @Override
    public int hashCode() {
        return siteId != null ? siteId.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "LogLatLong{" +
                "idLog=" + idLog +
                ", siteId='" + siteId + '\'' +
                ", type='" + type + '\'' +
                ", refNo='" + refNo + '\'' +
                ", latOriginal='" + latOriginal + '\'' +
                ", longOriginal='" + longOriginal + '\'' +
                ", latPosisi='" + latPosisi + '\'' +
                ", longPosisi='" + longPosisi + '\'' +
                ", jarak='" + jarak + '\'' +
                ", activityDateTime='" + activityDateTime + '\'' +
                ", apkVersion='" + apkVersion + '\'' +
                ", androidVersion='" + androidVersion + '\'' +
                ", modelNumber='" + modelNumber + '\'' +
                ", kernelVersion='" + kernelVersion + '\'' +
                ", buildNumber='" + buildNumber + '\'' +
                ", imei='" + imei + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", hasBeenSubmitted=" + hasBeenSubmitted +
                '}';
    }
}
