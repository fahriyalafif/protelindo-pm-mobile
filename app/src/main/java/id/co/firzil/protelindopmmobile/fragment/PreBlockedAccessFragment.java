package id.co.firzil.protelindopmmobile.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

import id.co.firzil.protelindopmmobile.R;
import id.co.firzil.protelindopmmobile.entity.TaskList;

/**
 * Created by Rio Rizky Rainey on 30/03/2015.
 * rizkyrainey@gmail.com
 */
public class PreBlockedAccessFragment extends BaseActivity {

    private TaskList taskList;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        taskList = (TaskList) getIntent().getExtras().getSerializable("taskList");
        setContentView(R.layout.fragment_pre_blocked);
        setNavbar();
        setTitle("PM Form");
        Button yesButton = (Button) findViewById(R.id.button_yes);
        Button noButton = (Button) findViewById(R.id.button_no);

        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taskList.setIsBlockedAccess(TaskList.BLOCKED_ACCESS);
                taskList.saveMerge();

                Intent intent = new Intent(PreBlockedAccessFragment.this, BlockedAccessFragment.class);
                intent.putExtra("idPm", taskList.getIdPm());
                startActivity(intent);
                finish();
            }
        });
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taskList.setIsBlockedAccess(TaskList.NOT_BLOCKED_ACCESS);
                taskList.saveMerge();

                Intent intent = new Intent(PreBlockedAccessFragment.this, PMFormFragment.class);
                intent.putExtra("taskList", taskList);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(1, R.id.restart_gps, 1, getString(R.string.action_restart_gps)).setEnabled(true);

        return true;
    }

}
