package id.co.firzil.protelindopmmobile;

/**
 * Created by Rio Rizky Rainey on 14/03/2015.
 * rizkyrainey@gmail.com
 */
public interface OnBackPressedListener {
    public void onBackPressed();
}
