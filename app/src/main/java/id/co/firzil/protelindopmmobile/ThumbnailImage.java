package id.co.firzil.protelindopmmobile;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import java.io.ByteArrayOutputStream;

public class ThumbnailImage {
		
	public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
	    int width = bm.getWidth();
	    int height = bm.getHeight();
	    float scaleWidth = ((float) newWidth) / width;
	    float scaleHeight = ((float) newHeight) / height;
	    // CREATE A MATRIX FOR THE MANIPULATION
	    Matrix matrix = new Matrix();
	    // RESIZE THE BIT MAP
	    matrix.postScale(scaleWidth, scaleHeight);

	    return Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
	}
	
	public Bitmap getThumbnail(Resources res, int id){
	    Bitmap imgthumBitmap=null;
	    try{
	    	final int THUMBNAIL_SIZE = 100;

	        imgthumBitmap = BitmapFactory.decodeResource(res, id);

	        imgthumBitmap = Bitmap.createScaledBitmap(imgthumBitmap,
	        		THUMBNAIL_SIZE, THUMBNAIL_SIZE, false);

	        ByteArrayOutputStream bytearroutstream = new ByteArrayOutputStream(); 
	        imgthumBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytearroutstream);

	     }
	     catch(Exception ex) {
	    	 ex.printStackTrace();
	     }
	     return imgthumBitmap;
	}
	
	private int getScale(int originalWidth,int originalHeight, final int reqWidth,final int reqHeight){
		 //a scale of 1 means the original dimensions
		 //of the image are maintained
		int inSampleSize = 1;

	    if (originalHeight > reqHeight || originalWidth > reqWidth) {

	        final int halfHeight = originalHeight / 2;
	        final int halfWidth = originalWidth / 2;

	        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
	        // height and width larger than the requested height and width.
	        while ((halfHeight / inSampleSize) > reqHeight
	                && (halfWidth / inSampleSize) > reqWidth) {
	            inSampleSize *= 2;
	        }
	    }
		       
		 return inSampleSize;
	}
		  
	private BitmapFactory.Options getOptions(String filePath, int requiredWidth,int requiredHeight){
		   
		 BitmapFactory.Options options=new BitmapFactory.Options();
		 //setting inJustDecodeBounds to true
		 //ensures that we are able to measure
		 //the dimensions of the image,without
		 //actually allocating it memory
		 options.inJustDecodeBounds=true;
		   
		 //decode the file for measurement
		 BitmapFactory.decodeFile(filePath,options);
		   
		 //obtain the inSampleSize for loading a
		 //scaled down version of the image.
		 //options.outWidth and options.outHeight
		 //are the measured dimensions of the
		 //original image
		 options.inSampleSize=getScale(options.outWidth,
		 options.outHeight, requiredWidth, requiredHeight);
		   
		 //set inJustDecodeBounds to false again
		 //so that we can now actually allocate the
		 //bitmap some memory
		 options.inJustDecodeBounds=false;
		   
		 return options;
		   
	}
	
	public Bitmap loadBitmap(String filePath, int requiredWidth,int requiredHeight){
		BitmapFactory.Options options = getOptions(filePath, requiredWidth, requiredHeight);
		   
		return BitmapFactory.decodeFile(filePath,options);
	}
		
	private BitmapFactory.Options getOptionsResource(Resources res, int idImage, int requiredWidth,int requiredHeight){
		   
		 BitmapFactory.Options options=new BitmapFactory.Options();
		 //setting inJustDecodeBounds to true
		 //ensures that we are able to measure
		 //the dimensions of the image,without
		 //actually allocating it memory
		 options.inJustDecodeBounds=true;
		   
		 //decode the file for measurement
		 BitmapFactory.decodeResource(res, idImage, options);
		   
		 //obtain the inSampleSize for loading a
		 //scaled down version of the image.
		 //options.outWidth and options.outHeight
		 //are the measured dimensions of the
		 //original image
		 options.inSampleSize=getScale(options.outWidth,
		 options.outHeight, requiredWidth, requiredHeight);
		   
		 //set inJustDecodeBounds to false again
		 //so that we can now actually allocate the
		 //bitmap some memory
		 options.inJustDecodeBounds=false;
		   
		 return options;
		   
	}
	
	public Bitmap loadBitmap(Resources res, int idImage, int requiredWidth,int requiredHeight){
		BitmapFactory.Options options = getOptionsResource(res, idImage, requiredWidth, requiredHeight);
		   
		return BitmapFactory.decodeResource(res, idImage, options);
	}
}
