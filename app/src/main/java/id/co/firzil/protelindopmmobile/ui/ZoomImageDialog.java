package id.co.firzil.protelindopmmobile.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import id.co.firzil.protelindopmmobile.CustomImageLoader;
import id.co.firzil.protelindopmmobile.R;
import pl.polidea.view.ZoomView;

/**
 * Created by Rio Rizky Rainey on 04/03/2015.
 * rizkyrainey@gmail.com
 */
public class ZoomImageDialog extends Dialog {

    private String path;

    private GreenButton editButton;

    public ZoomImageDialog(Activity activity, String path) {
        super(activity);
        this.path = path;
        init();
    }

    private void init() {
        CustomImageLoader imageLoader = new CustomImageLoader(getContext());
        ZoomView zoomView = new ZoomView(getContext());

        System.out.println("PATH DIALOG: " + path);

        View view = ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.zoom_image, new LinearLayout(getContext()), false);
        view.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        zoomView.addView(view);
        zoomView.setMaxZoom(5);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        setContentView(zoomView);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        ImageView imageView = (ImageView) findViewById(R.id.imageView1);
        editButton = (GreenButton) findViewById(R.id.button_editPhoto);
        imageLoader.displayImage(path, imageView, findViewById(R.id.progressBar1));
    }

    public void setEditButtonClickListener(View.OnClickListener editButtonClickListener) {
        editButton.setOnClickListener(editButtonClickListener);
    }
}
