package id.co.firzil.protelindopmmobile;

/**
 * Created by Rio Rizky Rainey on 15/04/2015.
 * rizkyrainey@gmail.com
 */
public interface EndlessScrollListener {
    public abstract void onLoadMore(int page, int totalItemsCount);
}
