package id.co.firzil.protelindopmmobile.fragment;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import id.co.firzil.protelindopmmobile.R;
import id.co.firzil.protelindopmmobile.entity.TaskList;

/**
 * Created by Rio Rizky Rainey on 13/02/2015.
 * rizkyrainey@gmail.com
 */
public class PMMapFragment extends BaseActivity implements OnMapReadyCallback{

    private final String TAG = "PM Map";

    private Double longitude;
    private Double latitude;
    private String name;
    private String powerType;
    private TaskList taskList;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        latitude = getIntent().getExtras().getDouble("lat");
        longitude = getIntent().getExtras().getDouble("long");
        name = getIntent().getExtras().getString("name");
        powerType = getIntent().getExtras().getString("powerType");
        taskList = (TaskList) getIntent().getExtras().getSerializable("tasklist");

        setContentView(R.layout.fragment_pm_map);
        setNavbar();
        setTitle(TAG);
        ((TextView) findViewById(R.id.textView_map)).setText(taskList.getIdSite() + "\n" + taskList.getName());
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();
        mapFragment.getMapAsync(this);
    }

//    @Override
//    public String getTagFragment() {
//        return TAG;
//    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng towerPosition = new LatLng(latitude, longitude);

        googleMap.setMyLocationEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(towerPosition, 13));

        googleMap.addMarker(new MarkerOptions()
                .title(name)
                .snippet(powerType)
                .position(towerPosition));
    }

    public void setLongitude(String longitude) {
        this.longitude = Double.parseDouble(longitude);
    }

    public void setLatitude(String latitude) {
        this.latitude = Double.parseDouble(latitude);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPowerType(String powerType) {
        this.powerType = powerType;
    }

    public void setTaskList(TaskList taskList) {
        this.taskList = taskList;
    }
}
