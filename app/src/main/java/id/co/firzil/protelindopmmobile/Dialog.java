package id.co.firzil.protelindopmmobile;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by Rio Rizky Rainey on 02/07/2015.
 * rizkyrainey@gmail.com
 */
public class Dialog {

    private Context context;
    private String text;
    private OnDismiss onDismiss;

    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;

    public Dialog(Context context, String title, String text) {
        this.context = context;
        this.text = text;
        builder = new AlertDialog.Builder(context);
        builder.setMessage(text);
        builder.setTitle(title);
        builder.create();
    }

    public void show() {
        alertDialog = builder.show();
    }

    public void setCancelable(boolean cancelable) {
        builder.setCancelable(cancelable);
    }

    public void dismiss() {
        alertDialog.dismiss();
        onDismiss.onDismiss();
    }

    public boolean isShowing() {
        return alertDialog != null && alertDialog.isShowing();
    }

    public void setOnDismiss(OnDismiss onDismiss) {
        this.onDismiss = onDismiss;
    }

    public void setTrueButton(String text, DialogInterface.OnClickListener listener) {
        builder.setPositiveButton(text, listener);
    }

    public interface OnDismiss {
        abstract void onDismiss();
    }

}
