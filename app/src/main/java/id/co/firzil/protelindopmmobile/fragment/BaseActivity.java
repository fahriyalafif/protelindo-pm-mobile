package id.co.firzil.protelindopmmobile.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gz.databaselibrary.ServiceHandler;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import id.co.firzil.aksesserverlib.AksesDataServer;
import id.co.firzil.protelindopmmobile.CustomImageLoader;
import id.co.firzil.protelindopmmobile.DashboardActivity;
import id.co.firzil.protelindopmmobile.Dialog;
import id.co.firzil.protelindopmmobile.GPSUtils;
import id.co.firzil.protelindopmmobile.Me;
import id.co.firzil.protelindopmmobile.R;
import id.co.firzil.protelindopmmobile.TimeDetector;
import id.co.firzil.protelindopmmobile.URL;
import id.co.firzil.protelindopmmobile.Updater;
import id.co.firzil.protelindopmmobile.Utils;

public class BaseActivity extends AppCompatActivity implements
        NavigationDrawerFragment.NavigationDrawerCallbacks, TimeDetector.Listener {

    private ActionBarDrawerToggle actionBarDrawerToggle;
    protected TextView latlongText;
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    private CharSequence mTitle;


    private List<ServiceHandler> handlerList;
    public static boolean getGpsTime;
    protected static long gpsTime = 0;
    protected Dialog dialogTime;
    protected static double lat, lon;
    protected Context c;
    private GPSActionReceiver glr = new GPSActionReceiver();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handlerList = new ArrayList<>();
        lat = TimeDetector.getInstance().getLatitude();
        lon = TimeDetector.getInstance().getLongitude();
        c = this;

        LocalBroadcastManager.getInstance(c).registerReceiver(glr, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
    }

    public void setNavbar() {
        CustomImageLoader imageLoader = new CustomImageLoader(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_dashboard);

        latlongText = (TextView) findViewById(R.id.text_latlong);

        setSupportActionBar(toolbar);
        mTitle = getTitle();
        ImageView imageIcon = (ImageView) findViewById(R.id.image_icon);

        imageIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BaseActivity.this, ProfileFragment.class));
            }
        });

        imageLoader.displayImage(URL.URL_IMAGE + Me.getValue(this, Me.AVATAR), imageIcon);

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.app_name, R.string.back) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                setTitle(mTitle);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                setTitle("Back");
            }
        };
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                drawerLayout, actionBarDrawerToggle);

        //mNavigationDrawerFragment.selectItem(0);
    }

    private Updater u;
    public void onResume(){
        super.onResume();
        TimeDetector.getInstance().addUpdateListener(this);

        if(Me.isLogin(this)) {
            Utils.checkAutoTime(c);
            if (GPSUtils.isGPSEnabled(this)) startGpsService();
            else {
                stopGpsService();
                GPSUtils.showDialogEnableGPS(this);
            }

            if (u == null) {
                u = new Updater(this);
                u.setUrlApiVersionChecker(URL.URL_VERSION_CHECKER);
            }
            u.cekVersion();
        }
        else if(! isFinishing()) finish();

    }

    private class GPSActionReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context cc, Intent in) {
            if (in.getAction().equals(LocationManager.PROVIDERS_CHANGED_ACTION)) {
                try{
                    if(GPSUtils.isGPSEnabled(c)) startGpsService();
                    else {
                        stopGpsService();
                        GPSUtils.showDialogEnableGPS(c);
                    }
                }
                catch(Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
            actionBar.setDisplayShowTitleEnabled(true);
        }
        setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.restart_gps:
                if (GPSUtils.isGPSEnabled(this)) {
                    stopGpsService();
                    startGpsService();
                    Utils.makeToast(this, "GPS restarted");
                } else GPSUtils.showDialogEnableGPS(this);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    protected void startGpsService() {
        TimeDetector.getInstance().start();
    }

    protected void stopGpsService() {
        TimeDetector.getInstance().stop();
        lat = TimeDetector.getInstance().getLatitude();
        lon = TimeDetector.getInstance().getLongitude();
        try {
            latlongText.setText("UNKNOWN LOCATION");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addServiceHandler(ServiceHandler serviceHandler) {
        handlerList.add(serviceHandler);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for (ServiceHandler serviceHandler : handlerList) {
            serviceHandler.destroyAsync();

        }
        LocalBroadcastManager.getInstance(c).unregisterReceiver(glr);
    }

    public void setTitle(String title) {
        ((TextView) findViewById(R.id.title)).setText(title);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        switch (position) {
            case 0:
                Intent in = new Intent(this, DashboardActivity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(in);

                break;

            case 1:
                Intent in2 = new Intent(this, ProfileFragment.class);
                in2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(in2);
                break;

            case 2:
                if(Me.isLogin(this)){

                    new AsyncTask<String, String, String>(){
                        private final String ID_USER = Me.getValue(c, Me.IDVENDOR_USER);
                        private final int MAX_ATTEMPT = 3;
                        private int attempt = 1;

                        @Override
                        protected String doInBackground(String... strings) {
                            if(attempt <= MAX_ATTEMPT && Utils.isOnline(BaseActivity.this)){
                                AksesDataServer ak = new AksesDataServer();
                                ak.addParam("APIKEY", "2D4FC5AC43BFB5B46E722EF762E24");
                                ak.addParam("iduser", ID_USER);
                                ak.addParam("regid", "");
                                JSONObject j = ak.proceedResultJSONObject(URL.UPDATE_REGID, "POST");
                                try {
                                    j.getInt("flag");
                                }
                                catch (Exception e) {
                                    e.printStackTrace();
                                    attempt++;
                                    doInBackground(strings);
                                }
                            }
                            return null;
                        }
                    }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                Me.logout(this);
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                finish();
                break;

        }
    }

    public static double getLat(){
        return lat;
    }

    public static double getLon(){
        return lon;
    }

    public static long getGpsTime(){
        return gpsTime;
    }

    @Override
    public final void onUpdate(final long paramLong1, final long paramLong2, final long paramInt, double latitude, double longitude) {
        lat = latitude;
        lon = longitude;
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //Log.i("Update time: ", "" + Utils.miliSecondToDateString(paramLong2));
                    gpsTime = paramLong2;
                    if (lat == 1000 & lon == 1000) {
                        getGpsTime = false;
                        if(latlongText != null) latlongText.setText("UNKNOWN LOCATION");
                    }
                    else {
                        getGpsTime = true;
                        if(latlongText != null) latlongText.setText(lat + ", " + lon);

                        if (dialogTime != null && dialogTime.isShowing()) {
                            dialogTime.dismiss();
                        }
                    }

                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }
}
