package id.co.firzil.protelindopmmobile;

import android.content.Context;
import android.content.SharedPreferences;

import com.gz.databaselibrary.GzApplication;

/**
 * Created by Rio Rizky Rainey on 19/03/2015.
 * rizkyrainey@gmail.com
 */
public class PMApplication extends GzApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferences.Editor editor = getSharedPreferences("instabug", Context.MODE_PRIVATE).edit();
        editor.putBoolean("ib_first_run", false);
        editor.commit();
        TimeDetector.getInstance().setActivity(this.getApplicationContext());
    }
}
