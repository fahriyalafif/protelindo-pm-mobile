package id.co.firzil.protelindopmmobile.fragment;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.EditText;

import com.gz.databaselibrary.ConnectionListener;
import com.gz.databaselibrary.ServiceHandler;

import org.json.JSONException;
import org.json.JSONObject;

import id.co.firzil.protelindopmmobile.Me;
import id.co.firzil.protelindopmmobile.R;
import id.co.firzil.protelindopmmobile.Tag;
import id.co.firzil.protelindopmmobile.URL;
import id.co.firzil.protelindopmmobile.Utils;

/**
 * Created by Rio Rizky Rainey on 16/02/2015.
 * rizkyrainey@gmail.com
 */
public class ProfileFragment extends BaseActivity implements View.OnClickListener {

    private EditText nameEditText;
    private EditText emailEditText;
    private EditText phoneEditText;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_profile);
        setNavbar();
        String TAG = "Profile";
        setTitle(TAG);
        nameEditText = (EditText) findViewById(R.id.editText_name);
        emailEditText = (EditText) findViewById(R.id.editText_email);
        phoneEditText = (EditText) findViewById(R.id.editText_phone);
        findViewById(R.id.button_update).setOnClickListener(this);

        nameEditText.setText(Me.getValue(ProfileFragment.this, Me.FULLNAME));
        emailEditText.setText(Me.getValue(ProfileFragment.this, Me.EMAIL));
        phoneEditText.setText(Me.getValue(ProfileFragment.this, Me.PHONE));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(1, R.id.restart_gps, 1, getString(R.string.action_restart_gps)).setEnabled(true);

        return true;
    }

    @Override
    public void onClick(View v) {
        ServiceHandler serviceHandler = new ServiceHandler(ProfileFragment.this, getResources().getString(R.string.tunggu));
        serviceHandler.addParam(Tag.IDUSER, Me.getValue(ProfileFragment.this, Me.IDVENDOR_USER));
        serviceHandler.addParam(Tag.NAME, nameEditText.getText().toString());
        serviceHandler.addParam(Tag.EMAIL, emailEditText.getText().toString());
        serviceHandler.addParam(Tag.PHONE, phoneEditText.getText().toString());
        serviceHandler.setConnectionListener(new ConnectionListener() {
            @Override
            public void onBeforeRequest() {

            }

            @Override
            public void onAfterRequested(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.optInt("flag") == 1) {
                        Me.commit(ProfileFragment.this, Me.FULLNAME, nameEditText.getText().toString());
                        Me.commit(ProfileFragment.this, Me.EMAIL, emailEditText.getText().toString());
                        Me.commit(ProfileFragment.this, Me.PHONE, phoneEditText.getText().toString());
                    }
                    Utils.makeToast(ProfileFragment.this, jsonObject.optString(Tag.MSG, ""));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        serviceHandler.makeServiceCallAsyncTask(URL.UPDATE_PROFILE, ServiceHandler.POST);
    }
}
