package id.co.firzil.aksesserverlib;

import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import org.json.JSONObject;

public class JSONParser {
	
	public String makeHttpRequest(String url, String method, ArrayList<KeyValuePair> params, ArrayList<KeyValuePair> headers) {
        return proceed(url, method, params, headers, null);
    }

    public String makeHttpRequest(String url, String method, ArrayList<KeyValuePair> headers, String stringParam) {
        return proceed(url, method, new ArrayList<KeyValuePair>(), headers, stringParam);
    }

    private String proceed(String url, String method, ArrayList<KeyValuePair> params, ArrayList<KeyValuePair> headers, String stringParam){
        String json = "";
        try {
            if(method.equalsIgnoreCase("GET") && params != null && (! params.isEmpty())){
                String paramString = getQuery(params);
                url += "?" + paramString;
            }

            Log.d("JP URL", "JP URL = "+url);
            URL urlx = new URL(url);
            HttpURLConnection con = (HttpURLConnection) urlx.openConnection();

            if(headers != null && headers.size() > 0){
                for(int i=0; i<headers.size(); i++){
                    KeyValuePair sp = headers.get(i);
                    con.setRequestProperty(sp.name, sp.value);
                }
            }

            if(! method.equalsIgnoreCase("GET")){
                String formParam = TextUtils.isEmpty(stringParam) ? getQuery(params) : stringParam;
                Log.d("JP FORM PARAM", "JP FORM PARAM = "+formParam);

                con.setRequestMethod(method);
                con.setDoInput(true);
                OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
                wr.write(formParam);
                wr.flush();
            }
            else con.setDoOutput(false);

            con.connect();

            StringBuilder sb = new StringBuilder();
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getResponseCode() == HttpURLConnection.HTTP_OK ?
                            con.getInputStream() : con.getErrorStream(), "utf-8"));
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
            br.close();
            json = sb.toString();
            Log.d("JP "+method, "JP = "+json);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return json;
    }

    private String getQuery(ArrayList<KeyValuePair> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (KeyValuePair pair : params){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.name, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.value, "UTF-8"));
        }

        return result.toString();
    }
    
	public JSONObject uploadImage(String url, ArrayList<KeyValuePair> listStringParam, ArrayList<KeyValuePair> listFileParam){
        JSONObject the_json = null;
        try {
            FileUploader multipart = new FileUploader(url);

            if(listFileParam != null && listFileParam.size() > 0) {
                for (int i = 0; i < listFileParam.size(); i++) {
                    KeyValuePair fp = listFileParam.get(i);
                    multipart.addFilePart(fp.name, new File(fp.value));
                }
            }

            if(listStringParam != null && listStringParam.size() > 0){
                for (int i=0; i<listStringParam.size(); i++){
                    KeyValuePair sp = listStringParam.get(i);
                    multipart.addFormField(sp.name, sp.value);
                }
            }

            String json = multipart.finish();

            //Log.d("upload", "upload = "+json);
            the_json = new JSONObject(json);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return the_json;
    }
    
}