package id.co.firzil.aksesserverlib;

import java.util.ArrayList;
import org.json.JSONObject;

public class PostDataWithImage {
	private ArrayList<KeyValuePair> stringParams = new ArrayList<>();
	private ArrayList<KeyValuePair> fileParams = new ArrayList<>();
	
	public void addStringParam(String nama_param, String value_param){
		stringParams.add(new KeyValuePair(nama_param, value_param));
	}
	
	public void addFileParam(String param_name, String path_file){
		fileParams.add(new KeyValuePair(param_name, path_file));
	}
	
	public JSONObject send(String url){
		return new JSONParser().uploadImage(url, stringParams, fileParams);
	}
}
