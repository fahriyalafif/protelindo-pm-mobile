package id.co.firzil.aksesserverlib;

public class KeyValuePair {
	public String name = "";
	public String value = "";
	
	public KeyValuePair(String nama, String value){
		this.name = nama;
		this.value = value;
	}
}
