package id.co.firzil.aksesserverlib;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AksesDataServer {
	
	private ArrayList<KeyValuePair> params = new ArrayList<>();
	private ArrayList<KeyValuePair> headerParams = new ArrayList<>();

	public void addParam(String nama_param, String value_param) {
		params.add(new KeyValuePair(nama_param, value_param));
	}

	public void addHeader(String name, String value){
		headerParams.add(new KeyValuePair(name, value));
	}

	public JSONObject proceedResultJSONObject(String url, String method) {
		JSONObject j = null;
		try {
			j = new JSONObject(proceedReturnString(url, method));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return j;
	}

	public JSONArray proceedReturnJSONArray(String url, String method) {
		JSONArray j = null;
		try {
			j = new JSONArray(proceedReturnString(url, method));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return j;
	}

	public String proceedReturnString(String url, String method) {
		return new JSONParser().makeHttpRequest(url, method, params, headerParams);
	}

	public String proceedReturnString(String url, String method, String stringParam) {
		return new JSONParser().makeHttpRequest(url, method, headerParams, stringParam);
	}

}
