package com.gz.databaselibrary;

public class TableOnline {

	public static final String QUEUE = "CREATE TABLE IF NOT EXISTS queue (id_queue INTEGER PRIMARY KEY,"
			+ " action TEXT NOT NULL, status INTEGER DEFAULT 0);";

	public static final String QUEUE_PARAM = "CREATE TABLE IF NOT EXISTS queue_parameter (id_queue_parameter "
			+ "INTEGER PRIMARY KEY,id_queue INTEGER NOT NULL,name TEXT,value TEXT);";

}
