package com.gz.databaselibrary;

public interface AsyncTaskListener {

	public abstract void sendResult(String data);
	
}
