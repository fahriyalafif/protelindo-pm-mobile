package com.gz.databaselibrary;

public interface ConnectionListener {

	public abstract void onBeforeRequest();
	public abstract void onAfterRequested(String response);

}
