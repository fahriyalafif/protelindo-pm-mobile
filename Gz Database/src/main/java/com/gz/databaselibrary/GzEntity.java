package com.gz.databaselibrary;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.gz.databaselibrary.annotation.Column;
import com.gz.databaselibrary.annotation.Id;
import com.gz.databaselibrary.annotation.Table;

import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static com.gz.databaselibrary.GzApplication.getInstance;

public class GzEntity implements Serializable {

    /**
     * versionUID
     */
    private static final long serialVersionUID = -8594432892186377783L;
    /**
     * class name. Tag for debugging.
     */
    protected final String TAG = getClass().getName();

    /**
     * delete from database
     *
     * @param classType   class of entity that contains the annotation {@link Table}
     * @param whereClause the optional WHERE clause to apply when deleting. Passing null
     *                    will delete all rows.
     * @param whereArgs   You may include ?s in the where clause, which will be replaced
     *                    by the values from whereArgs. The values will be bound as
     *                    Strings.
     * @return the number of rows affected if a whereClause is passed in, 0
     * otherwise. To remove all rows and get a count pass "1" as the
     * whereClause.
     */
    public static long delete(Class<?> classType, String whereClause, String... whereArgs) {
        return GzDatabase.getInstance().delete(classType.getAnnotation(Table.class).name(), whereClause, whereArgs);
    }

    /**
     * select all from the class that contains the annotation {@link Table}
     *
     * @param classType class of entity that contains the annotation {@link Table}
     * @return
     */
    public static <T> List<T> toArrayList(Class<?> classType) {
        String tableName = classType.getAnnotation(Table.class).name();
        String sql = "SELECT * FROM " + tableName;
        Cursor cursor = getInstance().getOpenHelper().getDatabase().rawQuery(sql, null);
        List<T> list = new ArrayList<T>();
        try {
            while (cursor.moveToNext()) {
                @SuppressWarnings("unchecked")
                T entity = (T) Utils.setEntityFromCursor(classType, cursor);
                list.add(entity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return list;
    }

    /**
     * SELECT entity by id
     *
     * @param classType class of entity that contains the annotation {@link Table}
     * @param id        String id
     * @return null if entity not found
     */
    @SuppressWarnings("unchecked")
    public static <T> T findById(Class<?> classType, String id) {
        String tableName = Utils.getTableAnnotation(classType).name();
        Field primary = Utils.getIdFromClass(classType);
        String sql = "SELECT * FROM " + tableName + " WHERE " + primary.getAnnotation(Column.class).name() + "=?";
        Cursor cursor = getInstance().getOpenHelper().getDatabase().rawQuery(sql, new String[]{id});
        T entity = null;
        try {
            while (cursor.moveToNext()) {
                entity = (T) Utils.setEntityFromCursor(classType, cursor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return entity;
    }

    /**
     * @param classType  class of entity that contains the annotation {@link Table}
     * @param columnName name of column argument
     * @param value
     * @return <code>true</code> if exist, <code>false</code> if otherwise
     */
    public static boolean isExist(Class<?> classType, String columnName, String... value) {
        String tableName = Utils.getTableAnnotation(classType).name();
        columnName = columnName.trim().replaceAll("\\s+", "");
        String[] arrayColumn = columnName.split(",");
        String columnText = "";
        for (int i = 0; i < arrayColumn.length; i++) {
            columnText += (arrayColumn[i] + "=? ");
            if (i != arrayColumn.length - 1) columnText += "AND ";
        }
        String sql = "SELECT COUNT(*) FROM " + tableName + " WHERE " + columnText;
        Cursor cursor = GzDatabase.getInstance().getDatabase().rawQuery(sql, value);
        int count = 0;
        while (cursor.moveToNext()) {
            count = cursor.getInt(0);
        }
        cursor.close();
        return count > 0;
    }

    public static <T> T findBy(Class<?> classType, String columnName, String value) {
        String tableName = Utils.getTableAnnotation(classType).name();
        Field primary = Utils.getIdFromClass(classType);
        columnName = columnName.trim().replaceAll("\\s+", "");
        value = value.trim().replaceAll("\\s+", "");
        String[] valueArray = value.split(",");
        String[] arrayColumn = columnName.split(",");
        String columnText = "";
        for (String anArrayColumn : arrayColumn) {
            columnText += (anArrayColumn + "=?");
        }
        String sql = "SELECT * FROM " + tableName + " WHERE " + columnText;
        Cursor cursor = getInstance().getOpenHelper().getDatabase().rawQuery(sql, valueArray);
        T entity = null;
        try {
            while (cursor.moveToNext()) {
                entity = (T) Utils.setEntityFromCursor(classType, cursor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return entity;
    }

    public static boolean isHasData(Class<? extends GzEntity> classType) {
        String tableName = Utils.getTableAnnotation(classType).name();
        String sql = "SELECT COUNT(*) FROM " + tableName;
        Cursor cursor = GzDatabase.getInstance().getDatabase().rawQuery(sql, null);
        int count = 0;
        while (cursor.moveToNext()) {
            count = cursor.getInt(0);
        }
        cursor.close();
        return count > 0;
    }

    /**
     * Init field from JSONObject. Field must contain annotation {@link Column}.
     * Only field with annotation {@link Column} to be processed.
     *
     * @param jsonObject
     */
    public void initFromJSONObject(JSONObject jsonObject) {
        Field[] attributes = getClass().getDeclaredFields();
        for (Field attribute : attributes) {
            Utils.setFieldFromJSONObject(attribute, jsonObject, this);
        }
    }

    /**
     * save entities to the database. The entity class must contain the
     * annotation {@link Table}. And has a field with annotation {@link Column}
     *
     * @return id save row
     */
    public long save() {
        Field[] attributes = getClass().getDeclaredFields();
        Object object = this;
        ContentValues values = Utils.setValueFromField(attributes, object);
        long id = GzDatabase.getInstance().insert(getClass().getAnnotation(Table.class).name(), values);
        if (id != -1) {
            Field primary = Utils.getIdFromClass(getClass());
            Cursor cursor = GzDatabase
                    .getInstance()
                    .getDatabase()
                    .rawQuery(
                            "SELECT " + primary.getAnnotation(Column.class).name() + " FROM "
                                    + getClass().getAnnotation(Table.class).name() + " WHERE rowid=?",
                            new String[]{"" + id});
            while (cursor.moveToNext()) {
                Utils.setFieldValueFromCursor(primary, cursor, object);
            }
        }
//        Log.d("saving entity", toString());
        return id;
    }

    /**
     * save merge. save if id not found. and update if id is exist
     */
    public void saveMerge() {
        if (isExist(getClass(), Utils.getIdFromClass(getClass()).getAnnotation(Column.class).name(), getId())) {
            update();
        } else {
            save();
        }
    }

    /**
     * update row by id. The entitu class must have a field which contains
     * annotation {@link Id}
     *
     * @return id update
     */
    public long update() {
        Field[] attributes = getClass().getDeclaredFields();
        Field primary = Utils.getIdFromClass(getClass());
        Object object = this;
        ContentValues values = Utils.setValueFromField(attributes, object);
//        Log.d("updating entity", toString());
        return GzDatabase.getInstance().update(getClass().getAnnotation(Table.class).name(), values,
                primary.getAnnotation(Column.class).name() + "=?", new String[]{Utils.fieldToString(primary, this)});
    }

    public long updateBy(String whereClause, String[] whereArgs) {
        Field[] attributes = getClass().getDeclaredFields();
        Field primary = Utils.getIdFromClass(getClass());
        Object object = this;
        ContentValues values = Utils.setValueFromField(attributes, object);
        Log.d("updating entity", toString());
        return GzDatabase.getInstance().update(getClass().getAnnotation(Table.class).name(), values,
                whereClause, whereArgs);
    }

    /**
     * returns the id field in the entity which contains annotation {@link Id}
     *
     * @return return id of entity.
     */
    public String getId() {
        Object object = null;
        Field primary = Utils.getIdFromClass(getClass());
        try {
            object = primary.get(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (object == null)
            return null;
        return object.toString();
    }

    @Override
    public String toString() {
        return getClass().toString() + "[id:" + getId() + "]";
    }
}
