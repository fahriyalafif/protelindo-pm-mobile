package com.gz.databaselibrary;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * 
 * @author Rio Rizky Rainey
 * class receiver. Start when network change.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

	public NetworkChangeReceiver() {
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		Intent i = new Intent(context, PostBackgroundService.class);
		System.out.println("MASUK NETWORK CHANGE");
		if (GzDatabase.isOnlineMode) {
			if (Utils.isOnLine(context)) {
				context.startService(i);
			} else {
				context.stopService(i);
			}
		}

	}

}
